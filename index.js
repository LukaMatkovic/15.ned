var App = (function (axios, Mustache) {
	var genres = [];
	var movies = [];

	var $genres = document.getElementById('genres');
	var $movies = document.getElementById('movies');
	var $movieWindow = document.getElementById('movieWindow');
	console.log($movieWindow);

	var genresTpl = document.getElementById('genres-template').innerHTML;
	var moviesTemplate = document.getElementById('movies-template').innerHTML;
	var windowTpl = document.getElementById('movieWindow-template').innerHTML;
	var shut = document.getElementById('shut');
	console.log(shut);


	return {
		getCurrentGenre: function () {
		},
		getMoviesByGenre: function () {
		},
		start: function () {
			var self = this;

			this.attachEvents();
			this.fetch()
			.then(function (response) {
				var data = response.data;
				genres = data.genres;
				movies = data.movies;

				self.createLinks();
			});
		},
		fetch: function (cb) {
			return axios.get('db.json');
		},
		createLinks: function () {
			$genres.innerHTML = Mustache.render(genresTpl, {
				genres: genres
			});
		},
		updateMovies: function () {

			var genreClick = location.hash.slice(2);
			console.log(genreClick);
			var movieArray = [];
			for(let i=0; i<movies.length; i++) {
				for (let j=0; j<movies[i].genres.length; j++) {
					if(movies[i].genres[j] == genreClick) {
							movieArray.push(movies[i]);
					}
				}
			$movies.innerHTML = Mustache.render(moviesTemplate, {
				movies: movieArray
			});
			for (let k=0; k<movieArray.length; k++) {
				var clickedMovie = document.getElementsByClassName('movie')[k];
				clickedMovie.addEventListener('click', function () {
					$movieWindow.classList.add('appear');
					$movieWindow.innerHTML = Mustache.render(windowTpl, {
						movies: movieArray[k]
					})
					shut.addEventListener('click', function () {
						$movieWindow.classList.remove('appear');
					});
					});
				}
			}
		},
		attachEvents: function () {
			window.addEventListener('hashchange', this.updateMovies.bind(this));
		}
	}
})(axios, Mustache);